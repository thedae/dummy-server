package lib

import (
	"dummy-server/lib/command"
	"fmt"
	"net"
	"strconv"
	"strings"
)

const defaultPort int = 9091

type Server struct {
	listener net.Listener
	clients  map[string]*Client
	rooms    []*Room
}

func NewServer() *Server {
	server := &Server{}

	server.clients = make(map[string]*Client)
	server.rooms = []*Room{}

	return server
}

func (server *Server) Listen() (bool, error) {
	listener, err := net.Listen("tcp4", ":"+strconv.Itoa(defaultPort))

	if err != nil {
		return false, err
	}

	server.listener = listener
	fmt.Println("[Listen] Listening on " + strconv.Itoa(defaultPort))

	return true, nil
}

func (server *Server) Shutdown() {
	fmt.Println("[Shutdown] Server got shutdown signal, disconnecting clients now")
	for _, client := range server.clients {
		client.Write("Server shutdown!")
		client.Close()
	}

	fmt.Println("[Shutdown] Closing listener")
	server.listener.Close()
	fmt.Println("[Shutdown] Bye")
}

func (server *Server) Serve() {
	shutdownSignal := make(chan bool)
	registerClient := make(chan *Client, 10)
	nextClient := make(chan bool)

	go func() {
		commandReader := command.NewServerCommandReader()
	inputLoop:
		for {
			c := commandReader.Read(EOL)
			if c.IsValid() {
				command := c.First
				switch {
				case strings.EqualFold(command, ServerShutdownCommand):
					break inputLoop
				case strings.EqualFold(command, ServerStatusCommand):
					fmt.Printf("[Status] Clients: %d\n", len(server.clients))
					fmt.Printf("[Status] Rooms: %d\n", len(server.rooms))
				case strings.EqualFold(command, ServerBroadcastCommand):
					for _, client := range server.clients {
						client.Write("[SERVER] " + strings.Join(c.Args, " "))
					}
				default:
					fmt.Println("[Input] Invalid command")
				}
			}
		}

		shutdownSignal <- true
	}()

	go server.AwaitClient(registerClient, nextClient)

serveLoop:
	for {
		select {
		case <-shutdownSignal:
			break serveLoop
		case client := <-registerClient:
			go server.HandleClient(client)
		case <-nextClient:
			go server.AwaitClient(registerClient, nextClient)
		}
	}
}

func (server *Server) AwaitClient(register chan *Client, next chan bool) {
	client, err := NewClient(server.listener)
	if err != nil {
		fmt.Println("[Serve] Could not establish new connection or listener was closed:", err)
	}

	register <- client
	next <- true
}

func (server *Server) RegisterClient(client *Client) {
	server.clients[client.GetId()] = client
}

func (server *Server) DisconnectClient(client *Client) {
	fmt.Println("[Client] Client disconnected", client.GetId())
	delete(server.clients, client.GetId())
	client.Close()

	client = nil
}

func (server *Server) HandleClient(client *Client) {
	fmt.Printf("[Client] New connection %s\n", client.GetId())

	server.RegisterClient(client)
	defer server.DisconnectClient(client)

	commandReader := command.NewClientCommandReader(client.connection)

clientLoop:
	for {
		c := commandReader.Read(EOL)
		if c.IsValid() {
			command := c.First
			switch {
			case strings.EqualFold(command, ClientExitCommand):
				client.Write("server> Bye")
				break clientLoop
			case strings.EqualFold(command, ClientHelpCommand):
				client.Write(HelpText)
			case strings.EqualFold(command, ClientAliasCommand):
				client.SetAlias(c.Args[0])
				client.Write("server> Alias changed to " + client.GetAlias())
			case strings.EqualFold(command, ClientJoinCommand):
				room := server.RegisterRoom(c.Args[0], client)
				if !room.IsMember(client) {
					room.Join(client, LevelBasic)
				}
			case strings.EqualFold(command, ClientSayCommand):
				room := server.GetRoomByName(c.Args[0])
				if room == nil {
					client.Write("server> Room #" + c.Args[0] + " not found")
				} else {
					if !room.IsMember(client) {
						client.Write("server> You are not a member of #" + room.Name)
					} else {
						room.Say(client, strings.Join(c.Args[0:], " "))
					}
				}
			default:
				client.Write("server> Invalid command, try /help")
			}
		}
	}
}

func (server *Server) RegisterRoom(name string, owner *Client) *Room {
	if room := server.GetRoomByName(name); room != nil {
		return room
	}

	freshRoom := NewRoom(name, owner)
	server.rooms = append(server.rooms, freshRoom)

	return freshRoom
}

func (server *Server) GetRoomByName(name string) *Room {

	for _, room := range server.rooms {
		if room.Name == name {
			return room
		}
	}
	return nil
}
