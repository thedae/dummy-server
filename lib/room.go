package lib

import "fmt"

type RoomMember struct {
	Client *Client
	Level  int
}

type Room struct {
	Name    string
	Members []*RoomMember
}

func NewRoom(name string, owner *Client) *Room {
	room := &Room{Name: name, Members: []*RoomMember{}}
	room.Join(owner, LevelOwner)

	return room
}

func (r *Room) Join(c *Client, level int) {
	roomMember := &RoomMember{
		Client: c,
		Level:  level,
	}

	fmt.Printf("old cap=%d len=%d\n", cap(r.Members), len(r.Members))
	r.Members = append(r.Members, roomMember)
	fmt.Printf("new cap=%d len=%d\n", cap(r.Members), len(r.Members))
}

func (r *Room) Say(who *Client, message string) {
	for _, member := range r.Members {
		if who != member.Client {
			member.Client.Write("MSG " + member.Client.GetAlias() + " " + message)
		}
	}
}

func (r *Room) IsMember(client *Client) bool {
	for _, member := range r.Members {
		if client == member.Client {
			return true
		}
	}
	return false
}
