package lib

import (
	"fmt"
	"net"
)

type Client struct {
	id         string
	alias      string
	connection net.Conn
}

func NewClient(listener net.Listener) (*Client, error) {
	c, err := listener.Accept()

	client := &Client{}
	client.connection = c

	if err != nil {
		return nil, err
	}

	client.id = client.GetAddr()
	client.alias = client.id

	return client, nil
}

func (client *Client) Close() {
	client.connection.Close()
}

func (client *Client) GetId() string {
	return client.id
}

func (client *Client) GetAddr() string {
	return client.connection.RemoteAddr().String()
}

func (client *Client) GetAlias() string {
	return client.alias
}

func (client *Client) SetAlias(alias string) {
	client.alias = alias
}

func (client *Client) Write(stream string) {
	_, err := client.connection.Write([]byte(stream + string(EOL)))

	if err != nil {
		fmt.Println("[Client] Write error", err)
	}
}
