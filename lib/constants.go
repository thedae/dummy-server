package lib

const EOL byte = '\n'

const (
	ClientExitCommand  string = "exit"
	ClientHelpCommand  string = "help"
	ClientAliasCommand string = "alias"
	ClientJoinCommand  string = "join"
	ClientSayCommand   string = "say"
)

const (
	ServerShutdownCommand  string = "shutdown"
	ServerStatusCommand    string = "status"
	ServerBroadcastCommand string = "bc"
)

const HelpText string = `
server> Available commands
server>		/alias <new>: Change alias
server>		/exit: Bye
server>		/help: This command
`
const (
	LevelBasic = iota
	LevelMod
	LevelOwner
)
