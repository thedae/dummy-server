package command

import (
	"bufio"
	"os"
)

type ServerCommandReader struct {
	reader *bufio.Reader
}

func NewServerCommandReader() *ServerCommandReader {
	return &ServerCommandReader{reader: bufio.NewReader(os.Stdin)}
}

func (r *ServerCommandReader) Read(eol byte) Command {
	input, _ := r.reader.ReadString(eol)

	return NewCommand(input)
}
