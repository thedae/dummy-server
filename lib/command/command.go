package command

import (
	"strings"
)

type Command struct {
	First    string
	Args     []string
	Original string
}

func NewCommand(input string) Command {
	inputString := strings.TrimSpace(input)

	pieces := strings.Split(inputString, " ")
	command := Command{Original: inputString}

	if len(pieces) > 0 {
		command.First = pieces[0]
	}

	if len(pieces) > 1 {
		command.Args = pieces[1:]
	}

	return command
}

func (c Command) IsValid() bool {
	return len(c.First) > 0
}

func (c Command) HasArgs() bool {
	return len(c.Args) > 0
}
