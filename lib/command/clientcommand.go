package command

import (
	"bufio"
	"net"
)

type ClientCommandReader struct {
	reader *bufio.Reader
}

func NewClientCommandReader(reader net.Conn) *ClientCommandReader {
	return &ClientCommandReader{reader: bufio.NewReader(reader)}
}

func (r *ClientCommandReader) Read(eol byte) Command {
	input, _ := r.reader.ReadString(eol)

	return NewCommand(input)
}
