package logger

import "fmt"

const (
	LogInfo    string = "INFO"
	LogWarning string = "WARNING"
	LogError   string = "ERROR"
)

func Log(context string, message string, level string) {
	fmt.Printf("[%s] %s | %s\n", level, context, message)
}

func Logf(context string, message string, level string, args ...interface{}) {
	printed := fmt.Sprintf(message, args)

	fmt.Printf("[%s] %s | %s\n", level, context, printed)
}
