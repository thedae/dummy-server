package main

import (
	"dummy-server/lib"
	"fmt"
	"os"
)

func main() {
	server := lib.NewServer()

	_, err := server.Listen()
	if err != nil {
		fmt.Println("[Error] Error binding to port:", err)
		os.Exit(1)
	}

	server.Serve()
	server.Shutdown()
}
